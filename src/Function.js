const checkTheUserBrowser = () => {
    const isSafari = window.safari !== undefined;
    if (isSafari) {
        return "not supported"
    }
}

export {
    checkTheUserBrowser
}