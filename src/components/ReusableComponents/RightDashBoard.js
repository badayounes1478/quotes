import React, { useContext, useState } from 'react'
import AppContext from '../../store/DataProvider'
import '../ReusableComponentsCss/RightDashBoard.css'
import constant from '../../constant';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';

const RightDashBoard = () => {

    const context = useContext(AppContext)
    const [deletedId, setdeletedId] = useState(null)

    const copyText = (text, author) => {
        let copyText = document.getElementById("copySaved");
        copyText.value = text + "  -" + author
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");
        toast("Quote copied", {
            autoClose: 1000,
            hideProgressBar: false,
        });
    }

    const deleteSavedQuote = async (id) => {
        try {
            if (!context.canEdit) return toast(`You have only view permission`, {
                autoClose: 2000,
                hideProgressBar: false,
            });

            const organizationId = localStorage.getItem('organizationId')
            setdeletedId(id)
            await axios.delete(`${constant.url}save/remove/${id}/${organizationId}`)
            setdeletedId(null)
            context.removeSavedQuote(id)
        } catch (error) {
            setdeletedId(null)
        }
    }

    const Loading = () => {
        return <div style={{ display: 'grid', gridTemplateColumns: 'repeat(1, 1fr)', gap: 10, marginTop: 10 }}>
            {
                [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20].map(data => {
                    return <div className='loading-card' key={data + "rightdaashboard"}>
                        <div className='skeleton' style={{ width: "100%", height: 10 }} />
                        <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 10 }} />
                        <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 10 }} />
                        <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 10 }} />
                    </div>
                })
            }
        </div>
    }

    return (
        <div className="right-dashboard">
            <input id="copySaved" type="text" value="" readOnly={true} />
            <div className="join-community">
                <span>Looking for a little assistance?</span>
                <a href="https://tawk.to/chat/615fde58157d100a41ab5e5d/1fhf7p2ht" target="_blank" rel="noopener noreferrer">
                    Live chat support
                </a>
                <a href="https://youtu.be/eS5tpAUEuzA" target="_blank" rel="noopener noreferrer">
                    Video Tutorial
                </a>
                <a href="https://www.facebook.com/groups/creatosaurus/" target="_blank" rel="noopener noreferrer">
                    Join our Facebook group
                </a>
            </div>
            <div className="recent-saved">
                <div className="head">
                    <span>Recent saved</span>
                    <span onClick={() => context.changeLeftSideBarAciveButton(3)}>See all</span>
                </div>
                <div className='scroll-container'>
                    {
                        context.savedQuotesLoading === true ? <Loading /> :
                            context.savedQuotes.length === 0 ? <div className='nothing-saved'>You haven't saved anything.</div> :
                                context.savedQuotes.map((data, index) => {
                                    if (index <= 5) {
                                        return <div className="card" key={index + "sr"}>
                                            <p>{data.quote}</p>
                                            <span>- {data.author}</span>
                                            <div className='button-container'>
                                                <svg onClick={() => copyText(data.quote, data.author)} xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                                                    <path d="M19.4 20H9.6C9.26863 20 9 19.7314 9 19.4V9.6C9 9.26863 9.26863 9 9.6 9H19.4C19.7314 9 20 9.26863 20 9.6V19.4C20 19.7314 19.7314 20 19.4 20Z" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                                    <path d="M15 9V4.6C15 4.26863 14.7314 4 14.4 4H4.6C4.26863 4 4 4.26863 4 4.6V14.4C4 14.7314 4.26863 15 4.6 15H9" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                                </svg>

                                                {
                                                    deletedId === data._id ? <div className='loader' /> :
                                                        <svg onClick={() => deleteSavedQuote(data._id)} xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                                                            <path d="M19 11V20.4C19 20.7314 18.7314 21 18.4 21H5.6C5.26863 21 5 20.7314 5 20.4V11" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                                            <path d="M10 17V11" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                                            <path d="M14 17V11" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                                            <path d="M21 7L16 7M3 7L8 7M8 7V3.6C8 3.26863 8.26863 3 8.6 3L15.4 3C15.7314 3 16 3.26863 16 3.6V7M8 7L16 7" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                                        </svg>
                                                }
                                            </div>
                                        </div>
                                    } else {
                                        return null
                                    }
                                })
                    }
                </div>
            </div>
            <ToastContainer />
        </div>
    )
}

export default RightDashBoard
